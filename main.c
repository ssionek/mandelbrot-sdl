#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <SDL2/SDL.h>

//Global vars
SDL_Color *palette;
Uint32 *pixels;
SDL_Window * window;
SDL_PixelFormat *format;
int window_size_x = 800;
int window_size_y = 600;
int max_iter = 64;
double zoom_val = 0.1;
char title[128] = {0};

//Mandelbrot viewport sides (can't be arsed to make a full struct \w functions - i.e do it properly)
double v_up = 1.12;
double v_down = -1.12;
double v_left = -2.0;
double v_right = 0.47;

double convert_to_scale(double scale_beg, double scale_end, int val, int val_max)
{
	double percent = (double)val/(double)val_max;
	double scale = scale_end-scale_beg;
	return scale_beg+(scale*percent);
}

void set_title()
{
	memset(title,'\0',256);
	snprintf(title,sizeof(char)*127,"mandelbrot-sdl (viewport: [%.2f,%.2f,%.2f,%.2f], max iterations: %d)",v_left,v_up,v_right,v_down,max_iter);
	SDL_SetWindowTitle(window,title);
}

void render_mandelbrot_frame()
{
	for(int y = 0; y < window_size_y; y++)
	{
		for(int x = 0; x < window_size_x; x++)
		{
			double x0 = convert_to_scale(v_left, v_right,x,window_size_x);
			double y0 = convert_to_scale(v_down, v_up,y,window_size_y);
			double calc_x = 0.0;
			double calc_y = 0.0;
			int iter = 0;
			double temp_x;
			while((calc_x*calc_x + calc_y*calc_y <= 2*2) && (iter < max_iter))
			{
				temp_x = calc_x*calc_x - calc_y*calc_y + x0;
				calc_y = 2*calc_x*calc_y + y0;
				calc_x = temp_x;
				iter += 1;
			}
			pixels[(y*window_size_x)+x] = SDL_MapRGBA(format,palette[iter].r,palette[iter].g,palette[iter].b,palette[iter].a);
		}
	}
}

void generate_palette()
{
	Uint8 r, g, b;
	for(int i = 0; i < max_iter; i++)
	{
		double c = ((double)i)/(max_iter-1);
		r = 255*(-11.5444*pow(c,5)+24.7079*pow(c,4)-14.4479*pow(c,3)+2.122875*pow(c,2)-0.13446*c+0.279996078);
		g = 255*(-1.21921*pow(c,4)+2.267498*pow(c,3)-1.73326*pow(c,2)+1.582772*c+0.001702159);
		b = 255*(26.2720929*pow(c,6)-65.3209*pow(c,5)+56.65624*pow(c,4)-19.2918*pow(c,3)+0.091963*pow(c,2)+1.386773*c+0.332663775);
		palette[i].r = r;
		palette[i].g = g;
		palette[i].b = b;
		palette[i].a = 255;
	}
}

int main(int argc, char **argv)
{
	SDL_Event event;
	bool should_render = false;
	bool quit = false;
	double zoom_x;
	double zoom_y;
	
	//warning: no checking
	if(argc > 1)
	{
		if(argc > 2)
		{
			window_size_x = atoi(argv[2]);
			window_size_y = atoi(argv[3]);
		}
		max_iter = atoi(argv[1]);
	}
	
	SDL_Init(SDL_INIT_VIDEO);
	window = SDL_CreateWindow("mandelbrot-sdl",SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_size_x, window_size_y, 0);
	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);
	SDL_Texture *texture = SDL_CreateTexture(renderer,SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, window_size_x, window_size_y);
	format = SDL_AllocFormat(SDL_PIXELFORMAT_ARGB8888);
	pixels = malloc(sizeof(Uint32)*window_size_x*window_size_y);
	palette = malloc(sizeof(SDL_Color)*max_iter);
	
	generate_palette();
	render_mandelbrot_frame();
	set_title();
	SDL_UpdateTexture(texture, NULL, pixels, window_size_x * sizeof(Uint32));
	
	while (!quit)
	{
		SDL_WaitEvent(&event);
		switch (event.type)
		{
			case SDL_KEYDOWN:
          		switch (event.key.keysym.sym) 
				{
            		case SDLK_ESCAPE:
						quit = true;
						break;
            		case SDLK_UP:
						zoom_y = (v_up-v_down);
						v_up -= 0.05f*zoom_y;
						v_down -= 0.05f*zoom_y;
						should_render = true;
              			break;
            		case SDLK_DOWN:
						zoom_y = (v_up-v_down);
						v_up += 0.05f*zoom_y;
						v_down += 0.05f*zoom_y;
						should_render = true;
              			break;
            		case SDLK_LEFT:
						zoom_x = (v_right-v_left)/3;
						v_left -= 0.05f*zoom_x;
						v_right -= 0.05f*zoom_x;
						should_render = true;
              			break;
            		case SDLK_RIGHT:
						zoom_x = (v_right-v_left)/3;
						v_left += 0.05f*zoom_x;
						v_right += 0.05f*zoom_x;
						should_render = true;
              			break;
					case SDLK_PLUS:
					case SDLK_EQUALS:
						zoom_x = (v_right-v_left)*zoom_val;
						zoom_y = (v_up-v_down)*zoom_val;
						v_left += zoom_x;
						v_right -= zoom_x;
						v_up -= zoom_y;
						v_down += zoom_y;
						should_render = true;
						break;
					case SDLK_MINUS:
						zoom_x = (v_right-v_left)*zoom_val;
						zoom_y = (v_up-v_down)*zoom_val;
						v_left -= zoom_val;
						v_right += zoom_val;
						v_up += zoom_val;
						v_down -= zoom_val;
						should_render = true;
						break;
          		}
          		break;
			case SDL_QUIT:
				quit = true;
				break;
		}

		//rendering
		if(should_render)
		{
			render_mandelbrot_frame();
			SDL_UpdateTexture(texture, NULL, pixels, window_size_x * sizeof(Uint32));
			set_title();
			should_render = false;
		}
		SDL_RenderClear(renderer);
		SDL_RenderCopy(renderer, texture, NULL, NULL);
		SDL_RenderPresent(renderer);
	}
	free(pixels);
	free(palette);
	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
