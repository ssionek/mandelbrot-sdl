# mandelbrot-sdl

A simple mandelbrot set viewer in C and SDL2. Written in a few hours, so don't expect much.

### Building
Linux and other *nixy environments: `gcc -lSDL2 -lm main.c`
Windows (through MSYS2): `gcc main.c -lSDL2main -lSDL2 -lm -mwindows -lmingw32 -DSDL_MAIN_HANDLED`
The Windows build would also need a static SDL2.dll to work outside of the MSYS2 environment.

### Usage
Move around with arrow keys, zoom in/out with -/+ 

Cmdline args (all are optional): `mandelbrot-sdl <iterations> <window width> <window height>`
